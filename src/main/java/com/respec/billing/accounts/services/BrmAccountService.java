package com.respec.billing.accounts.services;

import com.respec.billing.accounts.models.GenericAccount;
import com.respec.billing.accounts.models.brm.Account;
import com.respec.billing.accounts.repositories.BrmAccountRepository;
import com.respec.billing.common.Request;
import com.respec.billing.mappers.BrmAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BrmAccountService {

    private final BrmAccountRepository brmAccountRepository;
    private final BrmAccountMapper brmAccountMapper;

    @Autowired
    public BrmAccountService(BrmAccountRepository brmAccountRepository, BrmAccountMapper brmAccountMapper) {
        this.brmAccountRepository = brmAccountRepository;
        this.brmAccountMapper = brmAccountMapper;
    }

    public GenericAccount getAccount(final Request request) throws IOException, NoSuchAlgorithmException {

        Account account = brmAccountRepository.getAccountById(request.getTenantId(), request.getAccountId());
        // this is dookey but were just showing how it might work...
        return map(account);

    }


    public List<GenericAccount> listAccounts(final Request request) throws IOException, NoSuchAlgorithmException {

        List<Account> accounts = (List<Account>) brmAccountRepository.listAccountsByTenantId(request.getTenantId());
        return mapList(accounts);

    }


    private GenericAccount map(Account account) {

        return brmAccountMapper.convert(account);

    }


    private List<GenericAccount> mapList(List<Account> accounts) {

        List<GenericAccount> genericAccounts = new ArrayList<>();
        accounts.forEach((account) -> {
            genericAccounts.add(brmAccountMapper.convert(account));
        });

        return genericAccounts;

    }

}
