package com.respec.billing.accounts.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
//import com.respec.billing.accounts.models.gtv.BillingAccount;
import com.respec.it.billing.gtv.client.model.BillingAccount;

import com.respec.billing.accounts.models.GenericAccount;
import com.respec.billing.common.HttpResponseObject;
import com.respec.billing.common.Request;
import com.respec.billing.mappers.GtvAccountMapper;
import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class GtvAccountService {

    private final HttpClientService httpClientService;
    private final GtvAccountMapper gtvAccountMapper;

    @Value("${gtv-config.port}")
    private String port;
    @Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    @Value("${spring.application.name}")
    private String appName;
    @Value("${gtv-config.api-key}")
    private String gtvApiKey;
    @Value("${gtv-config.api.base.url}")  // GTV URL
    private String billingAccountsUrl;
    @Value("${gtv-config.api.path.billing-accounts}")  // billing-accounts path
    private String billingAccountsPath;
    @Value("${gtv-config.api.path.billing-account-categories}")  // billing-accounts-categories
    private String billingAccountCategories;

    @Autowired
    public GtvAccountService(HttpClientService httpClientService, GtvAccountMapper gtvAccountMapper) {
        this.httpClientService = httpClientService;
        this.gtvAccountMapper = gtvAccountMapper;
    }


    public GenericAccount getAccount(final Request request) throws IOException, NoSuchAlgorithmException, ServiceProcessingException {

        String url = billingAccountsUrl + String.format(billingAccountsPath, request.getPageNumber(), request.getPageSize(), request.getAccountId());
        this.httpClientService.initializeClient(this.connectTimeout);

        final HttpGet httpGet = new HttpGet(url);
        httpGet.setHeaders(this.createRequestHeaders(gtvApiKey));

        // responseObject is going to contain a GTV
        // representation of an Account that extends
        // BaseAccount...
        final HttpResponseObject responseObject = this.httpClientService.execute(httpGet);
        if(!responseObject.isSuccessful())
        {
            throw new ServiceProcessingException("Unsuccessful listAccounts() request to GTV endpoint.");
        }

        GenericAccount gAccount;
        try {
            final List<BillingAccount> accounts = mapJsonToList(responseObject);
            gAccount = map(accounts.get(0)); // yes, this is crap demo purposes only
        } catch(Exception ex) {
            throw new ServiceProcessingException("Error processing GTV getAccounts() results, account not found.", ex);
        }
        return gAccount;


        //final Response response = this.deserializeResponse(responseObject.getResponseString());
        //final Response response = new Response(responseObject.isSuccessful(), responseObject.getResponseString());


        //final Response response = new Response(responseObject.isSuccessful(), mapJsonToList(responseObject));
        /*
        final Response response = new Response();
        response.setSuccessful(responseObject.isSuccessful());
        response.setAccounts(accounts);
        return response;
        */
    }


    public List<GenericAccount> listAccounts(final Request request) throws IOException, NoSuchAlgorithmException, ServiceProcessingException {

        String url = billingAccountsUrl + String.format(billingAccountsPath, request.getPageNumber(), request.getPageSize(), request.getAccountId());
        this.httpClientService.initializeClient(this.connectTimeout);

        final HttpGet httpGet = new HttpGet(url);
        httpGet.setHeaders(this.createRequestHeaders(gtvApiKey));

        // responseObject is going to contain a GTV
        // representation of an Account that extends
        // BaseAccount...
        final HttpResponseObject responseObject = this.httpClientService.execute(httpGet);
        if(!responseObject.isSuccessful())
        {
            throw new ServiceProcessingException("Unsuccessful listAccounts() request to GTV endpoint.");
        }

        List<GenericAccount> gAccounts;
        try {
            final List<BillingAccount> accounts = mapJsonToList(responseObject);
            gAccounts = mapList(accounts);
        } catch(Exception ex) {
            throw new ServiceProcessingException("Error processing GTV listAccounts() results, accounts not found.", ex);
        }
        return gAccounts;

        //final Response response = this.deserializeResponse(responseObject.getResponseString());
        //final Response response = new Response(responseObject.isSuccessful(), responseObject.getResponseString());


        //final Response response = new Response(responseObject.isSuccessful(), mapJsonToList(responseObject));
        /*
        final Response response = new Response();
        response.setSuccessful(responseObject.isSuccessful());
        response.setAccounts(accounts);
        return response;
        */
    }

    private GenericAccount map(BillingAccount account) {

        //GtvAccountMapper mapper = Mappers.getMapper(GtvAccountMapper.class);
        return gtvAccountMapper.convert(account);

    }

    private List<GenericAccount> mapList(List<BillingAccount> accounts) {

        List<GenericAccount> genericAccounts = new ArrayList<>();
        //GtvAccountMapper mapper = Mappers.getMapper(GtvAccountMapper.class);
        accounts.forEach((account) -> {
            genericAccounts.add(gtvAccountMapper.convert(account));
        });

        return genericAccounts;

    }


    private Header[] createRequestHeaders(final String token) {

        return new Header[] {
                new BasicHeader("Accept", MediaType.APPLICATION_JSON_VALUE),
                new BasicHeader("Content-type", MediaType.APPLICATION_JSON_VALUE),
                new BasicHeader("X-Api-Key", token),
        };
    }


    private List<BillingAccount> mapJsonToList(HttpResponseObject responseObject) {

        Gson gson = new Gson();
        Type accountListType = new TypeToken<List<BillingAccount>>(){}.getType();
        List<BillingAccount> list = gson.fromJson(responseObject.getResponseString(), accountListType);

        return list;

    }


    private BillingAccount mapJsonToObject(HttpResponseObject responseObject) {

        Gson gson = new Gson();
        Type accountType = new TypeToken<BillingAccount>(){}.getType();
        BillingAccount account = gson.fromJson(responseObject.getResponseString(), accountType);

        return account;

    }


}
