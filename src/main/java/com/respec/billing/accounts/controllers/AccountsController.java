package com.respec.billing.accounts.controllers;

import com.respec.billing.accounts.models.GenericAccount;
import com.respec.billing.accounts.services.AccountService;
import com.respec.billing.common.Request;
import com.respec.billing.common.Response;
import com.respec.billing.accounts.contracts.AccountsProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountsController implements AccountsProxy
{
    private static final String DEFAULT_NOT_OK_STATUS_CODE = "UnhandledAccountServicesEx";

    @Value("${spring.application.name}")
    private String appName;

    @Value("${server.port}")
    private String port;

    private final EurekaInstanceConfigBean eurekaInstanceConfigBean;
    private final AccountService accountService;

    @Autowired
    public AccountsController(
            final EurekaInstanceConfigBean eurekaInstanceConfigBean,
            AccountService accountService)
    {
        this.eurekaInstanceConfigBean = eurekaInstanceConfigBean;
        this.accountService = accountService;
    }


    @RequestMapping(
            value = "/api/billing/account-services/{tenant}/get-account/{accountid}",
            method = RequestMethod.GET)
    public ResponseEntity<GenericAccount> getAccountById(
            @PathVariable(value = "tenant") final String tenantId,
            @PathVariable(value = "accountid") final int accountId,
            @RequestHeader HttpHeaders requestHeaders)
    {
        final long start = System.currentTimeMillis();

        try
        {
            Request request = new Request();
            request.setTenantId(tenantId);
            request.setAccountId(accountId);

            Response<GenericAccount> response = accountService.getAccount(requestHeaders, request);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Responded", "AccountsController");

            return ResponseEntity.ok().headers(responseHeaders).body(response.getAccounts().get(0));

        }
        catch (final Exception ex)
        {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Responded", "AccountsController");
            return ResponseEntity.badRequest().headers(responseHeaders).body(null);
        }

    }


    @Override
    @RequestMapping(
            value = "/api/billing/account-services/{tenant}/list-accounts",
            method = RequestMethod.GET)
    public ResponseEntity<List<GenericAccount>> listAccountsByTenantId(
            @PathVariable(value = "tenant") final String tenantId,
            @RequestHeader HttpHeaders requestHeaders) {

        final long start = System.currentTimeMillis();

        try
        {
            Request request = new Request();
            request.setTenantId(tenantId);

            Response<GenericAccount> response = accountService.listAccounts(requestHeaders, request);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Responded", "AccountsController");

            return ResponseEntity.ok().headers(responseHeaders).body(response.getAccounts());

        }
        catch (final Exception ex)
        {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Responded", "AccountsController");
            return ResponseEntity.badRequest().headers(responseHeaders).body(null);
        }

    }


}
