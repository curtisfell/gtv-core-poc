package com.respec.billing.accounts.models.brm;

import java.io.Serializable;

public class Account implements Serializable {

	private String accountNbr;
	private String parentAccountNbr;
	private String accountType;
	private int currencyType;

	public Account(){}

	public Account(String accountNbr, String parentAccountNbr, String accountType, int currencyType) {
		this.accountNbr = accountNbr;
		this.parentAccountNbr = parentAccountNbr;
		this.accountType = accountType;
		this.currencyType = currencyType;
	}

	public String getAccountNbr() {
		return accountNbr;
	}

	public void setAccountNbr(String accountNbr) {
		this.accountNbr = accountNbr;
	}

	public String getParentAccountNbr() {
		return parentAccountNbr;
	}

	public void setParentAccountNbr(String parentAccountNbr) {
		this.parentAccountNbr = parentAccountNbr;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public int getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(int currencyType) {
		this.currencyType = currencyType;
	}
}
