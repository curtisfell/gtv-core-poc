package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class RecurringPayment implements Serializable
{

    @SerializedName("payment_method")
    @Expose
    private PaymentMethod paymentMethod;
    @SerializedName("billing_account")
    @Expose
    private BillingAccount billingAccount;
    @SerializedName("valid_from")
    @Expose
    private String validFrom;
    @SerializedName("valid_to")
    @Expose
    private String validTo;
    @SerializedName("auto_payment")
    @Expose
    private String autoPayment;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 532303042202406256L;

    /**
     * No args constructor for use in serialization
     *
     */
    public RecurringPayment() {
    }

    /**
     *
     * @param autoPayment
     * @param paymentMethod
     * @param validFrom
     * @param id
     * @param billingAccount
     * @param validTo
     */
    public RecurringPayment(PaymentMethod paymentMethod, BillingAccount billingAccount, String validFrom, String validTo, String autoPayment, String id) {
        super();
        this.paymentMethod = paymentMethod;
        this.billingAccount = billingAccount;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.autoPayment = autoPayment;
        this.id = id;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BillingAccount getBillingAccount() {
        return billingAccount;
    }

    public void setBillingAccount(BillingAccount billingAccount) {
        this.billingAccount = billingAccount;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getAutoPayment() {
        return autoPayment;
    }

    public void setAutoPayment(String autoPayment) {
        this.autoPayment = autoPayment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentMethod", paymentMethod).append("billingAccount", billingAccount).append("validFrom", validFrom).append("validTo", validTo).append("autoPayment", autoPayment).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(autoPayment).append(paymentMethod).append(validFrom).append(id).append(billingAccount).append(validTo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RecurringPayment) == false) {
            return false;
        }
        RecurringPayment rhs = ((RecurringPayment) other);
        return new EqualsBuilder().append(autoPayment, rhs.autoPayment).append(paymentMethod, rhs.paymentMethod).append(validFrom, rhs.validFrom).append(id, rhs.id).append(billingAccount, rhs.billingAccount).append(validTo, rhs.validTo).isEquals();
    }

}