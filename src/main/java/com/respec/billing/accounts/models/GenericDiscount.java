package com.respec.billing.accounts.models;

import java.io.Serializable;

public class GenericDiscount implements Serializable {

    private String startDate;
    private String deactivationDate;
    private String duration;
    private String recurrence;
    private String value;
    private String applicationType;
    private String priceType;
    private String discountKey;
    private String id;

    public GenericDiscount() {
    }

    public GenericDiscount(String startDate, String deactivationDate, String duration, String recurrence, String value, String applicationType, String priceType, String discountKey, String id) {
        this.startDate = startDate;
        this.deactivationDate = deactivationDate;
        this.duration = duration;
        this.recurrence = recurrence;
        this.value = value;
        this.applicationType = applicationType;
        this.priceType = priceType;
        this.discountKey = discountKey;
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(String deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String recurrence) {
        this.recurrence = recurrence;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getDiscountKey() {
        return discountKey;
    }

    public void setDiscountKey(String discountKey) {
        this.discountKey = discountKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
