package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ResponsibleAccount implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("account_num")
    @Expose
    private String accountNum;
    @SerializedName("external_account_num")
    @Expose
    private Object externalAccountNum;
    private final static long serialVersionUID = 4652172564637471629L;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponsibleAccount() {
    }

    /**
     *
     * @param accountNum
     * @param externalAccountNum
     * @param id
     */
    public ResponsibleAccount(String id, String accountNum, Object externalAccountNum) {
        super();
        this.id = id;
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Object getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(Object externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("accountNum", accountNum).append("externalAccountNum", externalAccountNum).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(accountNum).append(externalAccountNum).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ResponsibleAccount) == false) {
            return false;
        }
        ResponsibleAccount rhs = ((ResponsibleAccount) other);
        return new EqualsBuilder().append(id, rhs.id).append(accountNum, rhs.accountNum).append(externalAccountNum, rhs.externalAccountNum).isEquals();
    }

}