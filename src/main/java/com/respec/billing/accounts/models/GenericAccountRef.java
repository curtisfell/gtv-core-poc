package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.respec.billing.accounts.models.gtv.CpqDiscount;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class GenericAccountRef implements Serializable
{
    private String accountNum;
    private String externalAccountNum;
    private List<GenericDiscount> genericDiscounts = null;
    private String id;
    private final static long serialVersionUID = 649055110452588068L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GenericAccountRef() {
    }

    /**
     *
     * @param accountNum
     * @param externalAccountNum
     * @param genericDiscounts
     * @param id
     */
    public GenericAccountRef(String accountNum, String externalAccountNum, List<GenericDiscount> genericDiscounts, String id) {
        super();
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
        this.genericDiscounts = genericDiscounts;
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(String externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    public List<GenericDiscount> getGenericDiscounts() {
        return genericDiscounts;
    }

    public void setGenericDiscounts(List<GenericDiscount> genericDiscounts) {
        this.genericDiscounts = genericDiscounts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("accountNum", accountNum).append("externalAccountNum", externalAccountNum).append("cpqDiscounts", genericDiscounts).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(genericDiscounts).append(id).append(accountNum).append(externalAccountNum).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof com.respec.billing.accounts.models.gtv.BillingAccountRef) == false) {
            return false;
        }
        GenericAccountRef rhs = ((GenericAccountRef) other);
        return new EqualsBuilder().append(genericDiscounts, rhs.genericDiscounts).append(id, rhs.id).append(accountNum, rhs.accountNum).append(externalAccountNum, rhs.externalAccountNum).isEquals();
    }

}