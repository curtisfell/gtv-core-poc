package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class MinimumInvoiceThreshold implements Serializable
{

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 1970004387077695440L;

    /**
     * No args constructor for use in serialization
     *
     */
    public MinimumInvoiceThreshold() {
    }

    /**
     *
     * @param amount
     * @param id
     * @param currencyCode
     */
    public MinimumInvoiceThreshold(String amount, String currencyCode, String id) {
        super();
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", amount).append("currencyCode", currencyCode).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(amount).append(id).append(currencyCode).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MinimumInvoiceThreshold) == false) {
            return false;
        }
        MinimumInvoiceThreshold rhs = ((MinimumInvoiceThreshold) other);
        return new EqualsBuilder().append(amount, rhs.amount).append(id, rhs.id).append(currencyCode, rhs.currencyCode).isEquals();
    }

}