package com.respec.billing.accounts.models.gtv;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BillingSchedule implements Serializable
{

    @SerializedName("billing_schedule_type")
    @Expose
    private String billingScheduleType;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 4251843906151389147L;

    /**
     * No args constructor for use in serialization
     *
     */
    public BillingSchedule() {
    }

    /**
     *
     * @param billingScheduleType
     * @param id
     */
    public BillingSchedule(String billingScheduleType, String id) {
        super();
        this.billingScheduleType = billingScheduleType;
        this.id = id;
    }

    public String getBillingScheduleType() {
        return billingScheduleType;
    }

    public void setBillingScheduleType(String billingScheduleType) {
        this.billingScheduleType = billingScheduleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("billingScheduleType", billingScheduleType).append("id", id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(billingScheduleType).append(id).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof BillingSchedule) == false) {
            return false;
        }
        BillingSchedule rhs = ((BillingSchedule) other);
        return new EqualsBuilder().append(billingScheduleType, rhs.billingScheduleType).append(id, rhs.id).isEquals();
    }

}