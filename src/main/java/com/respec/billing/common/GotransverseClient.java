package com.respec.billing.common;

import com.respec.it.billing.gtv.client.ApiClient;
import com.respec.it.billing.gtv.client.ApiException;
import com.respec.it.billing.gtv.client.Configuration;
import com.respec.it.billing.gtv.client.auth.*;
import com.respec.it.billing.gtv.client.api.BillingAccountApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GotransverseClient {

    @Value("${gtv-config.port}")
    private String port;
    @Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    @Value("${spring.application.name}")
    private String appName;
    @Value("${gtv-config.api-key}")
    private String gtvApiKey;
    @Value("${gtv-config.api.base.url}")  // GTV URL
    private String billingAccountsUrl;

    private ApiClient defaultClient;

    public GotransverseClient() {

        defaultClient = Configuration.getDefaultApiClient();
        ApiKeyAuth api_key_header = (ApiKeyAuth) defaultClient.getAuthentication("api_key_header");
        api_key_header.setApiKey(gtvApiKey);

        ApiKeyAuth defined_bearer_token = (ApiKeyAuth) defaultClient.getAuthentication("defined_bearer_token");
        defined_bearer_token.setApiKey(gtvApiKey);

        defaultClient.setApiKey(gtvApiKey);


    }

    public ApiClient getDefaultClient() {
        return defaultClient;
    }
}
