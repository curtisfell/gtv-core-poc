package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

//import com.respec.billing.accounts.models.gtv.BillingAccount;
import com.respec.it.billing.gtv.client.model.BillingAccount;
import com.respec.billing.accounts.models.GenericAccount;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvAccountMapper {

    @Mapping(source = "accountNum", target = "accountNumber")
    @Mapping(source = "externalAccountNum", target = "externalAccountNumber")
    @Mapping(source = "balance", target = "accountBalance")
    @Mapping(source = "parentBillingAccount.accountNum", target = "parentAccountId")
    @Mapping(source = "currencyCode", target = "currency")
    /* where is this in GTV */
    /* @Mapping(source = "", target = "billingAddress")*/
    /* @Mapping(source = "", target = "serviceAddress")*/
    @Mapping(source = "contacts", target = "contacts")
    /* where is this in GTV */
    /* @Mapping(source = "", target = "Bdom")*/
    @Mapping(source = "billType", target = "type")
    /* where is this in GTV */
    /* @Mapping(source = "", target = "paymentMethods") */
    @Mapping(source = "responsibleParty", target = "genericResponsibleParty")
    GenericAccount convert(BillingAccount source);

    //@InheritInverseConfiguration
    //BillingAccount targetToSource(GenericAccount target);

}
