package com.respec.billing.mappers;

import com.respec.billing.accounts.models.GenericDiscount;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

//import com.respec.billing.accounts.models.gtv.DefaultBillingAccount;
import com.respec.it.billing.gtv.client.model.BillingAccountRef;
import com.respec.billing.accounts.models.GenericAccountRef;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = { MapperUtility.class,  GtvCpqDiscountMapperImpl.class })
public interface GtvAccountRefMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "accountNum", target = "accountNum")
    @Mapping(source = "externalAccountNum", target = "externalAccountNum")
    @Mapping(source = "cpqDiscounts", target = "genericDiscounts")
    GenericAccountRef convert(BillingAccountRef source);

}
