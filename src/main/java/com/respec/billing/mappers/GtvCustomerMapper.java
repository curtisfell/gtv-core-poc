package com.respec.billing.mappers;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

//import com.respec.billing.accounts.models.gtv.Customer;
import com.respec.it.billing.gtv.client.model.Customer;
import com.respec.billing.accounts.models.GenericCustomer;
import org.springframework.core.convert.converter.Converter;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = MapperUtility.class)
public interface GtvCustomerMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "customerNum", target = "customerNum")
    @Mapping(source = "externalCustomerNum", target = "externalCustomerNum")
    @Mapping(source = "defaultBillingAccount", target = "defaultBillingAccount")
    @Mapping(source = "taxIdNumber", target = "taxIdNumber")
    @Mapping(source = "addresses", target = "addresses")
    @Mapping(source = "contactCategory", target = "contactCategory")
    GenericCustomer convert(Customer source);

    //@InheritInverseConfiguration
    //Customer targetToSource(GenericCustomer target);

}
