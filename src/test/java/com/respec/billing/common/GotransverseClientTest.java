package com.respec.billing.common;

import com.respec.billing.accounts.AccountsApplication;
import com.respec.it.billing.gtv.client.ApiClient;
import com.respec.it.billing.gtv.client.ApiException;
import com.respec.it.billing.gtv.client.api.BillingAccountApi;
import com.respec.it.billing.gtv.client.auth.ApiKeyAuth;
import com.respec.it.billing.gtv.client.model.BillingAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@EnableAutoConfiguration
class GotransverseClientTest {

    @Value("${gtv-config.port}")
    private String port;
    @Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    @Value("${spring.application.name}")
    private String appName;
    @Value("${gtv-config.api-key}")
    private String gtvApiKey;
    @Value("${gtv-config.api.base.url}")  // GTV URL
    private String billingAccountsUrl;

    @BeforeEach
    void setUp() { }

    @Test
    public void get_api_client_test() {

        GotransverseClient gtvc = new GotransverseClient();

        ApiClient apc = gtvc.getDefaultClient();
        apc.setApiKey(gtvApiKey);
        apc.setBasePath(billingAccountsUrl);

        ApiKeyAuth api_key_header = (ApiKeyAuth) apc.getAuthentication("api_key_header");
        api_key_header.setApiKey(gtvApiKey);

        ApiKeyAuth defined_bearer_token = (ApiKeyAuth) apc.getAuthentication("defined_bearer_token");
        defined_bearer_token.setApiKey(gtvApiKey);

        BillingAccountApi apiInstance = new BillingAccountApi(apc);
        List<String> expand = Arrays.asList("expand_example");
        try {
            BillingAccount result = apiInstance.getBillingAccount("97", "FULL", expand);
        }
        catch (ApiException e1) {
            int y = 0;
        }
        catch (Exception e2) {
            int y = 0;
        }

        int i = 0;

    }
}
